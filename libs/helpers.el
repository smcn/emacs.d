;;; helpers.el --- Misc functions which don't really fall into a specific category

;; Author: Stephen McNelly <stephen@hudson.co>
;; Maintainer: Stephen McNelly <stephen@hudson.co>
;; Created: 24 Mar 2021

(defun current-directory ()
  "This will return the current folder that you're in, not the entire filepath."
  (let* ((path (file-name-directory (buffer-file-name)))
	 (path-parts (split-string path "/")))
    (cadr (reverse path-parts))))

(defun check-dir-for (expected path)
  "If EXPECTED is a string, return true if the EXPECTED is found in the PATH.
If EXPECTED is a list, return true if all are found in the PATH."
  (let ((contents (directory-files path)))
    (cond ((stringp expected) (seq-contains contents expected))
	  ((listp expected) (= (length expected)
			       (seq-reduce (lambda (total cur-file)
					     (if (seq-contains contents cur-file)
						 (+ total 1) total))
					   expected 0))))))

(defun look-up-for (expected &optional path)
  "Travel up the directory tree starting at PATH, looking for EXPECTED (which can either be a string or a list of strings)."
  (let* ((path (if path path default-directory))
	 (path-parts (split-string path "/")))
    (unless (equal (length (delete "" path-parts)) 1)
      (if (check-dir-for expected path) path
	(look-up-for expected (string-join (reverse (cdr (reverse path-parts))) "/"))))))

(defun remove-nth-element (nth list)
  "Remove a specific element (NTH) from a LIST.
Code found here:
https://emacs.stackexchange.com/questions/29786/how-to-remove-delete-nth-element-of-a-list/29791#29791"
  (if (zerop nth) (cdr list)
    (let ((last (nthcdr (1- nth) list)))
      (setcdr last (cddr last))
      list)))

(defun capitalize-first-char (&optional string)
  "Capitalize only the first character of the input STRING."
  (when (and string (> (length string) 0))
    (let ((first-char (substring string nil 1))
	  (rest-str   (substring string 1)))
      (concat (capitalize first-char) rest-str))))

(defun xah-select-text-in-quote ()
  "Select text between the nearest left and right delimiters.
Delimiters here includes the following chars:
'\"`<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）This command
select between any bracket chars, not the inner text of a
bracket.  For example, if text is

 (a(b)c▮)

 the selected char is “c”, not “a(b)c”.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2020-03-11"
  (interactive)
  (let (
	($skipChars "^'\"`<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）〘〙")
	$p1
	)
    (skip-chars-backward $skipChars)
    (setq $p1 (point))
    (skip-chars-forward $skipChars)
    (set-mark $p1)))

(defun remove-x-elements (x l)
  (if (zerop x) l
    (remove-x-elements (1- x) (cdr l))))

(defun tramp-connection-p ()
  (file-remote-p default-directory))

(defun get-tramp-file-path (filepath)
  (third (split-string filepath ":")))
;;; helpers.el ends here
