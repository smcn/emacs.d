;;; hudson.el --- Functions for interacting with the Hudson Commerce tooling

;; Author: Stephen McNelly <stephen@hudson.co>
;; Maintainer: Stephen McNelly <stephen@hudson.co>
;; Created: 24 Mar 2021

(require 'dash)

(defun get-database-keys ()
  (-> (shell-command-to-string (concat smcn/work-folder "bin/database-password -keys -script"))
      (string-trim "(" ")")
      (split-string " ")))

(defun get-project-keys ()
  (-> (shell-command-to-string (concat smcn/work-folder "bin/projects"))
      (string-trim "(" ")")
      (split-string " ")))

(defun get-server-password (project)
  (kill-new (shell-command-to-string (concat smcn/work-folder "/bin/server-password " project))))

(defun hudson-database-passwords (&optional database)
  (interactive)
  (kill-new (shell-command-to-string
	     (concat smcn/work-folder "bin/database-password "
		     (if (null database)
			 (completing-read "Choose Database: " (get-database-keys))
		       database)
		     " -script")))
  (message "Copied!"))

(defun hudson-tramp ()
  (interactive)
  (let* ((project (completing-read "Please choose server: " (get-project-keys)))
	 (command-string (concat smcn/work-folder "bin/emacs-tramp " project)))
    (get-server-password project)
    (find-file (shell-command-to-string command-string))))

(defun vpn ()
  (interactive)
  (let ((server (read-string "Choose server: ")))
    (hudson-database-passwords server)
    (async-shell-command (concat "sudo pkill openvpn; cd /etc/openvpn && sudo openvpn dh"
				 server ".ovpn")
			 "*vpn*")))
;;; hudson.el ends here
