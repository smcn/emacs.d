;;; laravel.el --- Functions for manipulating Laravel installations

;; Author: Stephen McNelly <stephen@hudson.co>
;; Maintainer: Stephen McNelly <stephen@hudson.co>
;; Created: 24 Mar 2021

(defun get-laravel-root ()
  (look-up-for '("app" ".env" "routes" "database")))

(defun laravel-start ())
(defun laravel-stop ())
(defun ivy-artisan-console ())
(defun laravel-rebuild ())
(defun laravel-composer ())
(defun laravel-migrate ())
(defun laravel-make-model ())
(defun laravel-make-migration ())
(defun view-laravel-logs ())
(defun view-laravel-smcn-log-file ())
(defun laravel-routes-file ())
(defun sail-bash ())

;;; laravel.el ends here
