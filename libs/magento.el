;;; magento.el --- Functions for manipulating Magento 2 installations

;; Author: Stephen McNelly <stephen@hudson.co>
;; Maintainer: Stephen McNelly <stephen@hudson.co>
;; Created: 24 Mar 2021

(defun magento-remove-vendor-and-module (str)
  (string-join (remove-x-elements 2 (split-string str "\\\\")) "\\"))

(defun get-magento-root ()
  "Search up the directory tree looking for the first folder that contains the app, pub, and var directories."
  (look-up-for '("app" "pub" "var")))

(defun create-magento-module ()
  "Create a Magento module then open the module folder in current buffer."
  (interactive)
  (let ((root-dir (get-magento-root))
	(vendor (capitalize (read-string "Vendor Name: ")))
	(module (mapconcat 'identity (mapcar 'capitalize (split-string (read-string "Module Name: ") " ")) "")))
    (cd root-dir)
    (call-process (concat root-dir "/docker-bin/clinotty") nil "*magento-output*" nil
		  "bin/n98-magerun2.phar" "dev:module:create" vendor module)
    (dired (concat root-dir "/app/code/" vendor "/" module))))

(defun view-magento-logs ()
  "Open the var/logs folder in dired and sort by date modified."
  (interactive)
  (let ((root-dir (get-magento-root)))
    (dired (concat root-dir "/var/log/"))
    (dired-sort-toggle-or-edit)))

(defun view-smcn-log-file ()
  "Open and tail -f the smcn.log file."
  (interactive)
  (find-file-other-window (concat (get-magento-root) "/var/log/smcn.log"))
  (auto-revert-tail-mode)
  (goto-char (point-max)))

(defun view-hudson-modules ()
  "Open the Hudson module directory."
  (interactive)
  (dired (concat (get-magento-root) "app/code/Hudson")))

(defun get-magento-module-name ()
  "Use current directory to return Vendor_Module."
  (let ((parts (split-string (cadr (split-string default-directory "code")) "/")))
    (concat (cadr parts) "_" (caddr parts))))

(defun extend-magento-view-file ()
  "Extend the current view file into the appropriate theme folder."
  (interactive)
  (let* ((root-dir (get-magento-root))
	 (point-pos (point))
	 (filename (string-remove-prefix default-directory buffer-file-name))
	 (file-path (->> default-directory
			 (string-remove-prefix root-dir)
			 (string-remove-prefix "/vendor/")
			 (string-remove-prefix "/app/code/")))
	 (converted-path (replace-regexp-in-string "\\(view/frontend/\\).*\\'$" ""
						   (if (string= "magento" (substring file-path 0 7))
						       (concat "Magento_" (capitalize-first-char (substring file-path 15)))
						     (replace-regexp-in-string "\\(/\\).*\\'$" "_" file-path nil nil 1))
						   nil nil 1))
	 (theme-dir (read-directory-name "Choose theme directory: " (concat root-dir "/app/design/frontend"))))
    (clipboard-kill-ring-save (point-min) (point-max))
    (unless (file-directory-p (concat theme-dir converted-path))
      (make-directory (concat theme-dir converted-path) t))
    (find-file (concat theme-dir converted-path filename))
    (yank)
    (pop kill-ring)
    (goto-char point-pos)))

(defun open-work-project ()
  "Open the work directory to easily select a project."
  (interactive)
  (dired (read-directory-name "Choose Project: " (concat (getenv "HOME") "/Work")))
  (ggtags-mode)
  (gtags-mode))

(defun open-file-under-point ()
  "Find the file in the current string and open it, creating directories as required."
  (interactive)
  (let* ((converted-file (progn
			   (xah-select-text-in-quote)
			   (s-replace "\\" "/" (buffer-substring (region-beginning)
								 (region-end)))))
	 (magento? (s-starts-with? "Magento" converted-file))
	 (module-file (concat (get-magento-root)
			      (if magento?
				  "/vendor/magento/"
				"/app/code/")
			      (if magento?
				  (let ((parts (split-string converted-file "/")))
				    (mapconcat 'identity
					       (cons (concat "module-"
							     (s-replace-all '(("_" . "-"))
									    (s-snake-case (cadr parts))))
						     (cddr parts))
					       "/"))
				converted-file)
			      ".php")))
    (deactivate-mark)
    (find-file module-file)))

(defun php-construct ()
  "Convert the selected region into the appropriate format for a php __construct body."
  (interactive)
  (mapc (lambda (line)
	  (newline-and-indent)
	  (insert
	   (string-trim
	    (s-replace-regexp ".*\\$\\(.+\\).*?" "$this->\\1 = $\\1;" line))))
	(split-string (substring-no-properties (current-kill 0)) ",")))

(defun enable-magento-module ()
  "Enable the current module."
  (interactive)
  (let ((parts (split-string (string-remove-prefix (get-magento-root) default-directory) "/")))
    (imc-execute-command-with-args "module:enable" "bin/magento" (concat (fourth parts) "_" (fifth parts)))))

(defun disable-magento-module ()
  "Disable the current module."
  (interactive)
  (let ((parts (split-string (string-remove-prefix (get-magento-root) default-directory) "/")))
    (imc-execute-command-with-args "module:disable" "bin/magento" (concat (fourth parts) "_" (fifth parts)))))

(defun open-magento-env-file ()
  (interactive)
  (find-file-other-window (concat (get-magento-root) "/app/etc/env.php")))

(defun open-magento-config-file ()
  (interactive)
  (find-file-other-window (concat (get-magento-root) "/app/etc/config.php")))

(defun create-magento-rebuild-command ()
  (interactive)
  (let ((php  (completing-read "Version: " '("php" "php-7.0" "php-7.1" "php-7.2" "php-7.3")))
	(gulp (completing-read "Gulp Styles?: " '("NO" "YES"))))
    (kill-new (concat php " bin/magento maintenance:enable && "
		      php " bin/magento setup:upgrade && "
		      php " bin/magento setup:di:compile && "
		      php " bin/magento setup:static-content:deploy en_GB en_US -f && "
		      php " bin/magento cache:flush && "
		      php " bin/magento maintenance:disable"
		      (when (string= gulp "YES")
			" && cd tools && gulp styles && cd -")))))

(defun magento-project-note ()
  (interactive)
  (let* ((root (expand-file-name (get-magento-root)))
	 (parts (split-string root "/"))
	 (project (concat (fifth parts) ".org")))
    (find-file-other-window (concat smcn/work-folder "notes/" project))))

(global-set-key (kbd "C-c n") 'magento-project-note)

(defun run-hudson-docker-command (command)
  "Helper function to run any COMMAND in the $ROOT/docker-bin directory."
  (let ((current-directory default-directory)
	(magento-root (get-magento-root)))
    (cd magento-root)
    (async-shell-command (concat magento-root "/docker-bin/" command))
    (cd current-directory)))

(defun magento-rebuild ()
  "Run the docker-bin/rebuild command."
  (interactive)
  (run-hudson-docker-command "rebuild"))

(defun magento-reindex ()
  "Run the docker-bin/reindex command."
  (interactive)
  (run-hudson-docker-command "reindex"))

(defun magento-qbuild ()
  "Run the docker-bin/qbuild command."
  (interactive)
  (run-hudson-docker-command "qbuild"))

(defun magento-start ()
  "Run the docker-bin/start command."
  (interactive)
  (run-hudson-docker-command "start"))

(defun magento-stop ()
  "Run the docker-bin/stop command."
  (interactive)
  (run-hudson-docker-command "stop"))

(defun magento-composer ()
  "Run the docker-bin/composer command."
  (interactive)
  (run-hudson-docker-command (concat "composer " (read-string "Enter Composer Command: "))))

(defun docker-bash ()
  "Connect to the running docker container using TRAMP."
  (interactive)
  (let* ((root (expand-file-name (get-magento-root)))
	 (parts (split-string root "/"))
	 (container (concat (fifth parts) "_phpfpm_1")))
    (find-file-other-window (concat "/docker:" container ":/var/www/html"))))

(defun imc-execute-command (command console &optional php-version)
  "Execute the given COMMAND using the CONSOLE."
  (let ((console-location (if php-version
			      (concat php-version " " (get-tramp-file-path (get-magento-root)) console)
			    (concat (get-magento-root) "/docker-bin/clinotty " console))))
    (async-shell-command (concat console-location " " command))))

(defun imc-execute-command-with-args (command console args &optional php-version)
  "Execute the given COMMAND using the CONSOLE with the provided ARGS."
  (imc-execute-command (concat command " " args) console php-version))

(defun imc-execute-help-command (command console &optional php-version)
  "Take CONSOLE and return the help info for the currently highlighted COMMAND."
  (imc-execute-command (concat "help " command) console php-version))

(defun imc-create-list-of-commands (console &optional php-version)
  "Take the output of CONSOLE, parse it, and return a list of commands."
  (let* ((command-string (if php-version
			     (concat php-version " " (get-tramp-file-path (get-magento-root)) console)
			   (concat (get-magento-root) "/docker-bin/clinotty " console)))
	 (unsorted-list (split-string (shell-command-to-string command-string) "\n"))
	 (command-list '()))
    (dolist (el unsorted-list)
      (when (string-match "\\w:\\w" el)
	(push (s-replace-all '(("[32m" . "") ("[39m" . "")) el) command-list)))
    (prin1 (reverse command-list))))

(defun ivy-magento-console-command (console)
  "Execute CONSOLE commands using ivy."
  (let ((php-version (when (tramp-connection-p)
		       (completing-read "Version: " '("php" "php-7.0" "php-7.1" "php-7.2" "php-7.3")))))
    (ivy-read "Execute command: " (imc-create-list-of-commands console php-version)
	      :action '(1
			("e" (lambda (cmd)
			       (imc-execute-command (nth 2 (split-string cmd " ")) console php-version)) "Execute Command")
			("a" (lambda (cmd)
			       (imc-execute-command-with-args (nth 2 (split-string cmd " ")) console (read-string "Arguments(s): ") php-version)) "Execute Command with Arguments")
			("h" (lambda (cmd)
			       (imc-execute-help-command (nth 2 (split-string cmd " ")) console php-version)) "Display Help information")))))

(defun ivy-magento-console ()
  "Show a list of the available bin/magento commands in the Ivy minibuffer."
  (interactive)
  (ivy-magento-console-command "bin/magento"))

(defun ivy-n98magerun2-console ()
  "Show a list of the available bin/n98-magerun2.phar commands in the Ivy minibuffer."
  (interactive)
  (ivy-magento-console-command "bin/n98-magerun2.phar"))

;;; magento.el ends here
