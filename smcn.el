(defvar smcn/work-folder (concat (getenv "HOME") "/Work/"))

(add-to-list 'load-path "~/.emacs.d/libs/")
(load-library "helpers")
(load-library "magento")
(load-library "hudson")
(load-library "laravel")

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; (set-frame-font "Iosevka SS09-12")
;; (set-frame-font "Operator Mono")
;; (set-frame-font "Go Mono-11")
;; (set-frame-font "Fira Mono-11")
;; (set-frame-font "Inconsolata-13")
;; (set-frame-font "Monospace-11")
;; (set-frame-font "Ubuntu Mono-13")
;; (set-frame-font "IBM Plex Mono-11")
(set-frame-font "Cascadia Code-11")
;; (global-font-lock-mode 0)
;; (add-hook 'prog-mode-hook (lambda () (comments-only)) t)
(setq inhibit-startup-screen t)
(setq initial-major-mode 'lisp-interaction-mode)
(blink-cursor-mode 0)

(load-theme 'naysayer t)
;; (load-theme 'modus-operandi t)
;; (set-face-background 'fringe "white")

(global-set-key (kbd "C-h m") 'man)
(global-set-key (kbd "C-x f") 'ido-find-file-other-window)
(global-set-key (kbd "C-x \\") 'align-regexp)
(global-set-key (kbd "C-x C-z") nil)

(global-set-key (kbd "C-x 4 p") (lambda () (interactive) (scroll-other-window-down) (hydra-scroll-other-window/body)))
(global-set-key (kbd "C-x 4 n") (lambda () (interactive) (scroll-other-window) (hydra-scroll-other-window/body)))

(global-set-key (kbd "C-x +") (lambda () (interactive) (text-scale-increase 1) (hydra-text-resize/body)))
(global-set-key (kbd "C-x -") (lambda () (interactive) (text-scale-decrease 1) (hydra-text-resize/body)))

(defalias 'uniq 'delete-duplicate-lines)

(setq vc-follow-symlines t)
(setq make-backup-files nil)
(setq auto-save-default nil)
(global-subword-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
(prefer-coding-system 'utf-8)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

  (setq scroll-conservatively 100)
(setq ring-bell-function 'ignore)
;; (when window-system (global-hl-line-mode t))

;; Fixes OpenBSD hanging when Org Capture
;; (setq x-selection-timeout 10)

;; Sets temp file to /tmp
(set 'temporary-file-directory "/tmp/")

;; Use FreeDesktop.org's trash
(setq delete-by-moving-to-trash t)

(setq display-battery-mode nil)

;; ;; Adds TODO keyword to programming buffers
;; (add-hook 'prog-mode-hook
;; 	  (lambda ()
;; 	    (font-lock-add-keywords nil
;; 				    '(("\\<\\(TODO\\)" 1
;; 				       font-lock-warning-face t)))))

(add-hook 'prog-mode-hook 'show-paren-mode)
(add-hook 'prog-mode-hook 'electric-pair-local-mode)

(use-package rainbow-mode
  :ensure t
  :delight
  :init)

(global-set-key (kbd "<f12>") 'eshell)
(global-set-key (kbd "<mouse-9>") 'ibuffer)
(global-set-key (kbd "<mouse-8>") 'eshell)

(setq user-full-name "Stephen McNelly"
      user-mail-address "stephen@hudson.co")

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-default-smtp-server "smtp.office365.com"
      smtpmail-smtp-service 587
      smtpmail-local-domain "smcn-home")

(require 'smtpmail)

(global-set-key (kbd "C-x k") 'kill-this-buffer)
(global-set-key (kbd "C-x K") 'kill-buffer)

(defmacro smcn/define-keys (mode-map &rest keybindings)
  `(add-hook ,mode-map
	     '(lambda ()
		,@(mapcar (lambda (kb)
			    `(define-key ,(car kb) (kbd ,(cadr kb)) (lambda () (interactive) ,(caddr kb))))
			  keybindings))))

(with-eval-after-load 'gnus
 (define-key gnus-article-mode-map (kbd "C-c C-l") 'org-store-link))

(use-package hydra
  :ensure t
  :defer 2
  :config
  (defhydra hydra-phpunit (:hint nil)
    "
      ^
      ^PHPUnit^
      ^-------^---------------------
      _q_ quit      _p_ Test Project
      ^^  	        _f_ Test File
      ^^                ^^
      "
    ("q" nil)
    ("p" simplephpunit-test-project)
    ("f" simplephpunit-test-file)
    )

  (defhydra hydra-text-resize (:hint nil)
    "
      ^
      ^Resize text^
      ^-------^---------------------
      _q_ quit      _+_ Increase size
      ^^  	        _-_ Decrease size
      ^^                ^^
      "
    ("q" nil)
    ("+" text-scale-increase)
    ("-" text-scale-decrease)
    )

  ;; The hydra body will overlap window and so scroll won't work correctly,
  ;; that's why this body is blank.
  (defhydra hydra-scroll-other-window (:hint nil)
    "
  "
    ("q" nil)
    ("p" scroll-other-window-down)
    ("n" scroll-other-window)
    ("<" beginning-of-buffer-other-window)
    (">" end-of-buffer-other-window)
    )

  (defhydra hydra-magento (:hint nil :exit t)
    "
      ^
      ^Choose Magento Command^
      ^-------^---------------------
      _s_ Start             _S_ Stop
      _q_ Quick Build       _r_ Rebuild
      _c_ Composer          _i_ Reindex
      _m_ Magento Console   _n_ N98 Console
      _l_ View Logs         _L_ View SMCN Log
      _e_ Extend View       _f_ Open Module File
      _E_ Open Env File     _C_ Open Config File
      _b_ Bash              _H_ Hudson Modules
      ^^                ^^
      "
    ("s" magento-start)
    ("S" magento-stop)
    ("q" magento-qbuild)
    ("r" magento-rebuild)
    ("l" view-magento-logs)
    ("L" view-smcn-log-file)
    ("c" magento-composer)
    ("i" magento-reindex)
    ("m" ivy-magento-console)
    ("n" ivy-n98magerun2-console)
    ("e" extend-magento-view-file)
    ("f" open-file-under-point)
    ("E" open-magento-env-file)
    ("C" open-magento-config-file)
    ("b" docker-bash)
    ("H" view-hudson-modules)
    )

  (defhydra hydra-laravel (:hint nil :exit t)
    "
	^
	^Choose Laravel Command^
	^-------^---------------------
	_s_ Start             _S_ Stop
	_a_ Artisan           _r_ Rebuild
	_c_ Composer          _m_ Migrate
	_M_ Make Model        _d_ Make Migration
	_l_ View Logs         _R_ Routes File
        _b_ Bash
	^^                ^^
	"
    ("s" laravel-start)
    ("S" laravel-stop)
    ("a" ivy-artisan-console)
    ("r" laravel-rebuild)
    ("c" laravel-composer)
    ("m" laravel-migrate)
    ("M" laravel-make-model)
    ("d" laravel-make-migration)
    ("l" view-laravel-logs)
    ("R" laravel-routes-file)
    ("b" sail-bash)
    )

  (defhydra hydra-hudson (:hint nil :exit t)
    "
      ^
      ^Choose Hudson Command^
      ^-------^---------------------
      _d_ Database Passwords _p_ Work Projects
      _t_ Tramp (SSH)        _v_ VPN
      ^^                ^^
      "
    ("d" hudson-database-passwords)
    ("p" open-work-project)
    ("t" hudson-tramp)
    ("v" vpn)
    )

  (defhydra hydra-golang (:hint nil :exit t)
    "
      ^
      ^Golang^
      ^-------^---------------------
      _q_ quit      _p_ Test Project
      ^^  	        _f_ Test File
      ^^  	        _c_ Test Coverage
      ^^  	        _b_ Test Benchmark
      ^^  	        _B_ Test Project Benchmarks
      ^^                ^^
      "
    ("q" nil)
    ("p" go-test-current-project)
    ("f" go-test-current-file)
    ("c" go-test-current-coverage)
    ("b" go-test-current-file-benchmarks)
    ("B" go-test-current-project-benchmarks)))

(defun choose-project-hydra ()
  (interactive)
  (let ((project (current-project)))
    (cond
     ((equal project "magento") (hydra-magento/body))
     ((equal project "laravel") (hydra-laravel/body))
     (t (message "Project not defined")))))

(global-set-key (kbd "C-c m") 'choose-project-hydra)
;; (global-set-key (kbd "C-c m") 'hydra-magento/body)
(global-set-key (kbd "C-c h") 'hydra-hudson/body)

(use-package evil-ledger :ensure t :defer t)

(use-package ledger-mode
  :ensure t
  :defer t
  :mode "\\.dat\\'"
  :hook (ledger-mode-hook . evil-ledger-mode)
  :config
  (setq ledger-clear-whole-transactions 1))

(add-hook 'ledger-mode-hook
	  (lambda () (add-hook 'before-save-hook (lambda () (funcall ledger-mode-clean-buffer)))))

(require 'tramp)
;; (add-to-list 'tramp-methods
;; 	     '("doas"
;; 	       (tramp-login-program		"doas")
;; 	       (tramp-login-args		(("-u" "%u") ("-s")))
;; 	       (tramp-remote-shell		"/bin/sh")
;; 	       (tramp-remote-shell-args	("-c"))
;; 	       (tramp-connection-timeout	10)))

(use-package docker-tramp :ensure t :defer t)

(use-package avy
  :ensure t
  :defer 2
  :bind
  ("M-s" . avy-goto-char)
  ("M-S" . avy-goto-word-1))

(use-package swiper
  :ensure t
  :defer 2
  :bind
  ("C-s" . swiper))

(use-package ace-window
  :ensure t
  :delight
  :defer 2
  :init
  (setq aw-dispatch-always t)
  :bind
  ("M-o" . ace-window))

(use-package flycheck
  :ensure t
  :delight
  :defer 2
  :config
  (setq flycheck-phpcs-standard "PSR2")
  (global-flycheck-mode))

(use-package evil-surround
  :ensure t
  :defer 2
  :delight
  :init
  (global-evil-surround-mode 1))

(use-package evil-leader
  :ensure t
  :delight
  :config
  (global-evil-leader-mode)
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key

    ;; Window management
    ;; "w /" 'evil-window-vsplit
    ;; "w -" 'evil-window-split
    ;; "w d" 'evil-window-delete

    ;; Window Movement
    ;; "h" 'evil-window-left
    ;; "j" 'evil-window-down
    ;; "k" 'evil-window-up
    ;; "l" 'evil-window-right

    ;; EYEBROWSE management
    "," 'eyebrowse-prev-window-config
    "." 'eyebrowse-next-window-config
    "n" 'eyebrowse-create-window-config
    "x" 'eyebrowse-close-window-config

    "m" 'hydra-magento/body
    "h" 'hydra-hudson/body

    ;; Ace-Window management
    "o" 'ace-window
    )
  (evil-leader/set-key-for-mode 'php-mode "t" 'hydra-phpunit/body)
  (evil-leader/set-key-for-mode 'go-mode "t" 'hydra-golang/body))

(use-package evil
  :ensure t
  :defer 2
  :delight
  :config
  (add-to-list 'evil-emacs-state-modes 'elfeed-search-mode)
  (add-to-list 'evil-emacs-state-modes 'elfeed-show-mode)
  (add-to-list 'evil-emacs-state-modes 'cfw:calendar-mode)
  (add-to-list 'evil-emacs-state-modes 'cfw:details-mode)
  (add-to-list 'evil-emacs-state-modes 'dired-mode)
  (setq evil-insert-state-cursor 'nil)
  (evil-mode 1))

(define-key evil-normal-state-map (kbd "C-p") 'git-gutter:previous-hunk)
(define-key evil-normal-state-map (kbd "C-n") 'git-gutter:next-hunk)
(define-key evil-normal-state-map (kbd "M-.") nil)
(define-key evil-normal-state-map (kbd "C-e") 'move-end-of-line)
(define-key evil-normal-state-map (kbd "C-a") 'move-beginning-of-line)
(define-key evil-normal-state-map (kbd "C-t") 'transpose-chars)
(define-key evil-normal-state-map (kbd "C-y") 'yank)
(define-key evil-normal-state-map (kbd "C-x h") 'previous-buffer)
(define-key evil-normal-state-map (kbd "C-x l") 'next-buffer)

(define-key evil-insert-state-map (kbd "C-e") 'move-end-of-line)
(define-key evil-insert-state-map (kbd "C-a") 'move-beginning-of-line)
(define-key evil-insert-state-map (kbd "C-t") 'transpose-chars)
(define-key evil-insert-state-map (kbd "C-y") 'yank)
(define-key evil-insert-state-map (kbd "C-k") 'kill-line)
(define-key evil-insert-state-map (kbd "C-p") 'previous-line)
(define-key evil-insert-state-map (kbd "C-n") 'next-line)
(define-key evil-insert-state-map (kbd "C-x h") 'previous-buffer)
(define-key evil-insert-state-map (kbd "C-x l") 'next-buffer)

(define-key evil-visual-state-map (kbd "C-e") 'move-end-of-line)
(define-key evil-visual-state-map (kbd "C-a") 'move-beginning-of-line)
(define-key evil-visual-state-map (kbd "C-c s") 'send-region-to-counsel-rg)
(define-key evil-visual-state-map (kbd "C-s") 'send-region-to-counsel-grep-or-swiper)

;; Evil controls "C-w" really well, so this needs to be defined here
(define-key evil-window-map "\C-s" 'ivy-push-view)
(define-key evil-window-map "s" 'ivy-push-view)
(define-key evil-window-map "\C-d" 'ivy-pop-view)
(define-key evil-window-map "d" 'ivy-pop-view)
(define-key evil-window-map "." 'ivy-switch-view)

(use-package evil-matchit
  :ensure t
  :defer 2
  :delight
  :config
  (global-evil-matchit-mode 1))

(use-package evil-nerd-commenter
  :ensure t
  :defer 2
  :delight
  :bind
  ("M-;" . evilnc-comment-or-uncomment-lines)
  ("C-c l" . evilnc-copy-and-comment-lines)
  ("C-c P" . evilnc-comment-or-uncomment-paragraphs))

(use-package amx
  :ensure t
  :bind
  ("M-x" . amx))

(use-package counsel
  :ensure t
  :defer 2
  :delight
  :bind
  (("\C-s" . counsel-grep-or-swiper)
  ("C-x C-f" . counsel-find-file)
  ("C-x b" . counsel-ibuffer)
  ("C-x C-b" . counsel-recentf)
  ("C-h f" . counsel-describe-function)
  ("C-h v" . counsel-describe-variable)

  ;; Projectile replacement
  ("C-c g" . counsel-git)
  ("C-c s" . counsel-rg)
  ("C-c f" . counsel-file-jump)
  :map prog-mode-map
	("C-c /" . counsel-imenu)))

(ivy-mode 't)
 (use-package ivy
   :ensure t
   :defer 2
   :delight
   :bind
   ("C-M-s" . swiper-all)
   ("C-x B" . ivy-switch-buffer-other-window)
   :config
   (ivy-mode 1)
   (setq ivy-use-virtual-buffers t)
   (setq enable-recursive-minibuffers t)
   (setq ivy-initial-inputs-alist nil))

(setq dired-listing-switches "-alh")
(setq dired-dwim-target t)

(use-package eshell-git-prompt :ensure t :defer t)
(eshell-git-prompt-use-theme 'robbyrussell)
(require 'ffap)

(defmacro smcn/eshell-ffap (name &rest body)
  `(defun ,name ()
     (interactive)
     (let ((file (ffap-file-at-point)))
       (if file
	   ,@body
	 (user-error "No file at point")))))

(defun smcn/eshell-history ()
  (interactive)
  (let ((hist (ring-elements eshell-history-ring)))
    (insert
     (completing-read "History: " hist nil t))))

(smcn/eshell-ffap
 smcn/eshell-cat-file
 (progn
   (goto-char (point-max))
   (insert (concat "cat " file))
   (eshell-send-input)))

(smcn/eshell-ffap
 smcn/eshell-find-file-at-point
 (find-file file))

(defun smcn/eshell-last-output-to-buffer ()
  (interactive)
  (let ((eshell-output (buffer-substring-no-properties
			(eshell-beginning-of-output)
			(eshell-end-of-output))))
    (with-current-buffer (get-buffer-create "*eshell-output*")
      (erase-buffer)
      (insert eshell-output)
      (switch-to-buffer-other-window (current-buffer)))))

(defun smcn/eshell-redirect-to-buffer ()
  (interactive)
  (insert
   (format " >>> #<%s>"
	   (read-buffer-to-switch "Switch to buffer: "))))

;; (setq eshell-prompt-function
;; 	(lambda()
;; 	  (concat
;; 	   (propertize
;; 	    (if (= (user-uid) 0 ) "root#" "term%")
;; 	    'face `(:foreground "#67cd5d"))
;; 	   (propertize
;; 	    " "
;; 	    'face `(:foreground "#d2b58d")))))
;;(setq eshell-prompt-regexp "term%|root# ")

(smcn/define-keys 'eshell-mode-hook
		  (evil-visual-state-local-map "M-c" (smcn/eshell-cat-file))
		  (evil-insert-state-local-map "M-c" (smcn/eshell-cat-file))
		  (evil-normal-state-local-map "M-c" (smcn/eshell-cat-file))

		  (evil-visual-state-local-map "C->" (smcn/eshell-redirect-to-buffer))
		  (evil-insert-state-local-map "C->" (smcn/eshell-redirect-to-buffer))
		  (evil-normal-state-local-map "C->" (smcn/eshell-redirect-to-buffer))

		  (evil-visual-state-local-map "M-." (smcn/eshell-last-output-to-buffer))
		  (evil-insert-state-local-map "M-." (smcn/eshell-last-output-to-buffer))
		  (evil-normal-state-local-map "M-." (smcn/eshell-last-output-to-buffer))

		  (evil-visual-state-local-map "C-c C-j" (smcn/eshell-find-file-at-point))
		  (evil-insert-state-local-map "C-c C-j" (smcn/eshell-find-file-at-point))
		  (evil-normal-state-local-map "C-c C-j" (smcn/eshell-find-file-at-point))

		  (evil-visual-state-local-map "C-r" (smcn/eshell-history))
		  (evil-insert-state-local-map "C-r" (smcn/eshell-history))
		  (evil-normal-state-local-map "C-r" (smcn/eshell-history))

		  (evil-visual-state-local-map "M-r" (smcn/eshell-history))
		  (evil-insert-state-local-map "M-r" (smcn/eshell-history))
		  (evil-normal-state-local-map "M-r" (smcn/eshell-history))

		  (evil-visual-state-local-map "C-l" (eshell/clear-scrollback))
		  (evil-insert-state-local-map "C-l" (eshell/clear-scrollback))
		  (evil-normal-state-local-map "C-l" (eshell/clear-scrollback))

		  (evil-visual-state-local-map "M-p" (eshell-previous-input 1))
		  (evil-insert-state-local-map "M-p" (eshell-previous-input 1))
		  (evil-normal-state-local-map "M-p" (eshell-previous-input 1))

		  (evil-visual-state-local-map "M-n" (eshell-next-input 1))
		  (evil-insert-state-local-map "M-n" (eshell-next-input 1))
		  (evil-normal-state-local-map "M-n" (eshell-next-input 1)))

(setq eshell-cmpl-ignore-case t)
(setq eshell-history-size 10000)

(defun smcn/eshell-init ()
  (when (tramp-connection-p)
    (company-mode -1)))

(defun eshell-mode-hook-func ()
  (setq eshell-path-env (concat (getenv "HOME") "/Work/bin:" eshell-path-env))
  (setenv "PATH" (concat (getenv "HOME") "/Work/bin:" (getenv "PATH"))))

(add-hook 'eshell-mode-hook 'smcn/eshell-init)
(add-hook 'eshell-mode-hook 'eshell-mode-hook-func)

(use-package yasnippet
  :ensure
  :delight
  :defer 2
  :init
  (yas-global-mode 1)
  :bind
  ("S-<tab>" . yas-expand)
  ("M-C-s" . yas-insert-snippet)
  :config
  (setq yas-snippet-dir '("~/.emacs.d/snippets/")))

(use-package ggtags :ensure t :defer t)

(use-package geben
  :ensure t
  :defer t
  )
  (setq geben-path-mappings '(("/home/smcn/Work/craft56/" "/var/www/html/")))

(use-package company-php :ensure t :defer t)
;;(use-package php-doc :ensure t :defer t)
(use-package composer :ensure t :defer t)
(use-package ac-php :ensure t :defer t)
;;(require 'ivy-magento-console)
;;(require 'simplephpunit)

(use-package phpcbf :ensure t :defer t)

;; phpcbf
(custom-set-variables
 '(phpcbf-executable "~/.config/composer/vendor/bin/phpcbf")
 '(phpcbf-standard "PSR2"))

(use-package php-mode
  :ensure t
  :defer t
  :init
  (add-hook 'php-mode-hook 'phpcbf-enable-on-save)
  (add-hook 'php-mode-hook 'php-enable-psr2-coding-style)
  (add-hook 'php-mode-hook
	    '(lambda ()
	       (require 'company-php)
	       (ggtags-mode 1)
	       (company-mode t)
	       (ac-php-core-eldoc-setup) ;; enable eldoc
	       (make-local-variable 'company-backends)
	       (add-to-list 'company-backends 'company-ac-php-backend)))
  :bind (:map php-mode-map
	      ("C-c C--" . php-current-class)
	      ("C-c C-l" . org-store-link)
	      ("C-c C-=" . php-current-namespace)
	      ("C-c C-c" . ivy-magento-console)
	      ("C-c C-j" . ac-php-find-symbol-at-point)
	      ("C-c d" . php-search-local-documentation)
	      ("C-c C-d" . php-search-documentation)
	      ("C-c C-p f" . counsel-git)
	      ("C-c C-p C-f" . counsel-file-jump))
  :config
  (setq php-manual-path '"~/.man_pages/php-chunked-xhtml")
  (setq php-mode-coding-style 'psr2)
  (setq php-mode-lineup-cascaded-calls t)
  (setq browse-url-browser-function 'eww-browse-url)
  )
(add-hook 'php-mode-hook 'phpcbf-enable-on-save)

(use-package sly
  :ensure t
  :defer t
  :bind
  (("C-x C-b" . sly-eval-buffer)))

(setq inferior-lisp-program "/usr/local/bin/sbcl")
;; (setq inferior-lisp-program "/home/smcn/git-sources/ccl-dev/ccl/lx86cl")
(evil-set-initial-state 'sly-inspector-mode 'emacs)
(evil-set-initial-state 'sly-xref-mode 'emacs)
(evil-set-initial-state 'sly-apropos-mode 'emacs)
(evil-set-initial-state 'sly-trace-dialog-mode 'emacs)
(evil-set-initial-state 'sly-thread-control-mode 'emacs)
(evil-set-initial-state 'sly-connection-list-mode 'emacs)
(evil-set-initial-state 'sly-db-mode 'emacs)
(evil-set-initial-state 'sly--completion-display-mode 'emacs)
(evil-set-initial-state 'sly-stickers--replay-mode 'emacs)
;; (ql:quickload "clhs")
;; (clhs:install-clhs-use-local)
(load "/home/smcn/quicklisp/clhs-use-local.el" t)
(setq browse-url-browser-function
      '(("hyperspec" . eww-browse-url)
	("." . browse-url-default-browser)))

(evil-define-key 'insert sly-mrepl-mode-map (kbd ",") 'self-insert-command)
(evil-define-key 'emacs  sly-mrepl-mode-map (kbd ",") 'self-insert-command)
(evil-define-key 'normal sly-mrepl-mode-map (kbd ",") 'sly-mrepl-shortcut)
(evil-define-key 'visual sly-mrepl-mode-map (kbd ",") 'sly-mrepl-shortcut)

(add-to-list 'auto-mode-alist '("\\.rkt$" . racket-mode))
(use-package geiser
  :ensure t
  :defer t
  :config
(setq geiser-racket-binary "/usr/local/racket/bin/racket"))
(use-package racket-mode
  :ensure t
  :defer t
  :config
  (setq racket-program "/usr/local/racket/bin/racket"))

(setq geiser-active-implementations '(guile))

(use-package cider :ensure t :defer t)

(use-package paredit
  :ensure t
  :defer t
  :hook ((emacs-lisp-mode	. enable-paredit-mode)
	 (scheme-mode		. enable-paredit-mode)
	 (lisp-mode		. enable-paredit-mode)
	 (lisp-interaction-mode . enable-paredit-mode)))

(mapc
 (lambda (mode)
   (smcn/define-keys mode
		     (evil-insert-state-local-map "(") (paredit-open-round)
		     (evil-insert-state-local-map "M-)") (paredit-close-round-and-newline)
		     (evil-insert-state-local-map "{") (paredit-open-curly)
		     (evil-insert-state-local-map "M-}") (paredit-close-curly-and-newline)
		     (evil-insert-state-local-map "<") (paredit-open-angled)
		     (evil-insert-state-local-map "M->") (paredit-close-angled-and-newline)
		     (evil-insert-state-local-map "[") (paredit-open-square)
		     (evil-insert-state-local-map "M-]") (paredit-close-square-and-newline)

		     ;; slurping/barfing
		     (evil-insert-state-local-map "C-)") (paredit-forward-slurp-sexp)
		     (evil-insert-state-local-map "C-(") (paredit-backward-slurp-sexp)
		     (evil-insert-state-local-map "C-{") (paredit-forward-barf-sexp)
		     (evil-insert-state-local-map "C-}") (paredit-backward-barf-sexp)

		     ;; misc
		     (evil-insert-state-local-map "M-;") (paredit-comment-dwim)
		     (evil-insert-state-local-map "C-j") (paredit-newline)
		     (evil-insert-state-local-map "C-k") (paredit-kill)))
 '(emacs-lisp-mode scheme-mode lisp-mode lisp-interaction-mode))

(use-package haskell-mode :ensure t :defer t)

(use-package go-mode
  :ensure t
  :defer t
  :bind
  (:map go-mode-map
	("C-c M-f" . godoc-at-point)
	("M-TAB" . company-complete)
	("C-c 4 C-j" . godef-jump-other-window)
	("C-c d" . godoc)))
(add-hook 'go-mode-hook 'go-eldoc-setup)
(add-hook 'go-mode-hook (lambda ()
			  (set (make-local-variable 'company-backends) '(company-go))
			  (company-mode)))
(setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing
(add-hook 'before-save-hook 'gofmt-before-save)

(use-package go-eldoc
  :ensure t
  :defer t)

(use-package company-go
  :ensure t
  :defer t)

(use-package gotest
  :ensure t
  :defer t)

(use-package tide
  :ensure t
  :after (rjsx-mode company flycheck)
  :hook ((rjsx-mode . tide-setup)
	 ;; (rjsx-mode . tide-hl-identifier-mode)
	 (before-save . tide-format-before-save))
  :bind
  (:map tide-mode-map
	("C-c C-d" . tide-documentation-at-point)
	("C-c C-j" . tide-jump-to-definition))
  :config
  (setq js-indent-level 2))
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  ;; (tide-hl-identifier-mode +1)
  (company-mode +1))

(use-package rjsx-mode
  :ensure t)

(use-package js2-mode
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.js$" . rjsx-mode))

(use-package markdown-mode
:ensure t
:mode (("README\\.md\\'" . gfm-mode)
	("\\.md\\'" . markdown-mode)
	("\\.markdown\\'" . markdown-mode))
:init (setq markdown-command "multimarkdown"))

(use-package company
  :ensure t
  :defer 2
  :delight
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 3)
  (company-show-numbers t)
  (company-tooltip-align-annotations 't)
  (global-company-mode t))

(use-package restclient :ensure t :defer t)
(add-to-list 'company-backends 'company-restclient)

(use-package which-key
  :ensure t
  :defer 2
  ;;:delight
  :config
  (which-key-setup-minibuffer)
  (setq which-key-idle-delay 1.0)
  (which-key-mode))

(use-package browse-kill-ring
  :ensure t
  :delight
  :config
  (browse-kill-ring-default-keybindings)
  :bind
  (("M-y" . browse-kill-ring)
  :map browse-kill-ring-mode-map
  ("j" . browse-kill-ring-forward)
  ("k" . browse-kill-ring-previous)))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
	 :map magit-revision-mode-map
	 ("C-c C-l" . org-store-link))
  :config
  (setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1))

(use-package ivy-pass
  :ensure t
  :defer 2
  :bind
  ("C-c p" . ivy-pass))

(calendar-set-date-style 'european)

(global-set-key (kbd "C-c o c") 'calendar)
(global-set-key (kbd "C-c o C a") 'appt-add)
(global-set-key (kbd "C-c o C d") 'diary)
(global-set-key (kbd "C-c o C D") 'diary-mail-entries)

(setq appt-activate t)
(setq calendar-view-diary-initially-flag t)
(setq diary-mail-days '8)
(add-hook 'calendar-today-visible-hook 'calendar-mark-today)
(setq calendar-mark-diary-entries-flag t)
(add-hook 'diary-list-entries-hook 'diary-sort-entries t)

(setq calendar-longitude -4.2)
(setq calendar-latitude  55.8)
(setq calendar-location-name "Glasgow, UK")

(setq holiday-general-holidays nil)
(setq holiday-christian-holidays nil)
(setq holiday-hebrew-holidays nil)
(setq holiday-islamic-holidays nil)
(setq holiday-bahai-holidays nil)
(setq holiday-oriental-holidays nil)

(setq holiday-other-holidays
    '((holiday-fixed 1 1 "New Years Days")
	(holiday-fixed 2 14 "Valentine's Day")
	(holiday-fixed 3 17 "St. Patrick's Day")
	(holiday-fixed 4 1 "April Fools' Day")
	(holiday-easter-etc -47 "Shrove Tuesday")
	(holiday-easter-etc -21 "Mother's Day")
	(holiday-easter-etc -2 "Good Friday")
	(holiday-easter-etc 0 "Easter Sunday")
	(holiday-easter-etc 1 "Easter Monday")
	(holiday-float 5 1 1 "Early May Bank Holiday")
	(holiday-float 5 1 -1 "Spring Bank Holiday")
	(holiday-float 6 0 3 "Father's Day")
	(holiday-float 8 1 -1 "Summer Bank Holiday")
	(holiday-fixed 10 31 "Halloween")
	(holiday-fixed 12 24 "Christmas Eve")
	(holiday-fixed 12 25 "Christmas Day")
	(holiday-fixed 12 26 "Boxing Day")
	(holiday-fixed 12 31 "Hogmanay")))

(smcn/define-keys 'nxml-mode-hook
		  (evil-normal-state-local-map "C-c C-j" (open-file-under-point))
		  (evil-insert-state-local-map "C-c C-j" (open-file-under-point))
		  (evil-visual-state-local-map "C-c C-j" (open-file-under-point)))

(use-package org
  :ensure t
  :defer
  :bind (("C-c o a" . counsel-org-goto-all)
	  ("C-c a" . org-agenda)
	  ("C-c c" . org-capture)
	  ("C-c 4 b" . bookmark-jump-other-window)
	  ("C-c b" . counsel-bookmark)
	  :map org-mode-map
	  ("C-c C-q" . counsel-org-tag)
	  ("C-c /" . counsel-org-goto)
	  ("M-RET" . smcn/org-meta-return)
	  ("<M-S-return>" . smcn/org-insert-todo-heading)
	  ("C-c e" . org-edit-src-code))
  :config
  (setq org-html-validation-link nil)
  (setq org-agenda-files (list "~/org/dailyplanner.org"
			       "~/org/life.org"
			       "~/org/diary.org"
			       "~/org/todo.org"))
  (setq org-agenda-include-diary t)
  (setq org-agenda-start-on-weekday 0 )
  (setq org-directory "/home/smcn/org/")
  (setq org-default-notes-file (concat org-directory "/notes.org"))
  (setq org-archive-location "~/org/archive.org::* From %s")
  (setq org-return-follows-link t)
  (setq org-ellipsis "...")
  (setq org-export-preserve-breaks t)
  (setq org-log-into-drawer t)
  (setq org-capture-templates '(
				("j" "Journal Entry"
				 entry (file+datetree "~/org/journal.gpg")
				 "* %<%H:%M>\n%?"
				 )
				("l" "Log Time"
				 entry (file+datetree "~/org/log.org")
				 "** %U - %^{Activity}  :TIME:"
				)
				("d" "Diary Entry"
				 entry (file+headline "~/org/diary.org" "Reminder")
				 "** %?")
				)))

(use-package orgit :ensure t :defer t)

(use-package ox-ssh :ensure t)

(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "RET") nil))

(use-package olivetti
  :ensure t
  :defer t)

(setq TeX-view-program-list '(("Evince" "evince --page-index=%(outpage) %o")))
(setq TeX-view-program-selection '((output-pdf "Evince")))
(use-package latex-preview-pane
  :ensure t
  :defer t)

(use-package docker
  :ensure t
  :defer)
(evil-set-initial-state 'docker-container-mode 'emacs)
(evil-set-initial-state 'docker-image-mode 'emacs)
(evil-set-initial-state 'docker-network-mode 'emacs)
(evil-set-initial-state 'docker-volume-mode 'emacs)
(evil-set-initial-state 'docker-machine-mode 'emacs)

(global-set-key (kbd "C-c d") 'docker)

(use-package eyebrowse
  :ensure t
  :defer
  :delight
  :init
  (eyebrowse-mode t))

(use-package web-mode
  :ensure t
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.twig\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-hook 'web-mode-hook
	    '(lambda ()
	       (ggtags-mode 1)))
  :config
  (setq web-mode-engines-alist
	'(("php"     . "\\.phtml\\'")
	  ("blade"   . "\\.blade\\.")
	  ("django"  . "\\.twig\\.")
	  ("erb"     . "\\.erb\\.")))
  (setq web-mode-extra-auto-pairs
	'(("erb"  . (("beg" "end")))
	  ("php"  . (("beg" "end")
		     ("beg" "end")))))
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-block-face t)
  (setq web-mode-enable-part-face t)
  (setq web-mode-enable-comment-keywords t)
  (setq web-mode-enable-heredoc-fontification t)
  (setq web-mode-script-padding 4)
  (setq web-mode-style-padding 4)
  (setq web-mode-block-padding 4))

(use-package nginx-mode :ensure t :defer t)
(use-package company-nginx
  :ensure t
  :config
  (eval-after-load 'nginx-mode
    '(add-hook 'nginx-mode-hook #'company-nginx-keywords)))

(use-package emmet-mode
  :ensure t
  :defer 2
  :delight
  :init
  (add-hook 'web-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook 'emmet-mode)
  :config
  (setq emmet-expand-jsx-className? t))

(define-derived-mode smcn/plantuml-mode fundamental-mode "plantuml"
  "Major mode for editing PlantUML buffers.")

(add-to-list 'auto-mode-alist '("\\.puml\\'" . smcn/plantuml-mode))

(defun compile-plantuml-to-png ()
  (interactive)
  (let ((filename buffer-file-name))
    (save-buffer)
    (shell-command (concat "plantuml -tpng " filename))
    (find-file-other-window (concat (file-name-sans-extension filename) ".png"))))

(define-key smcn/plantuml-mode-map (kbd "C-c C-c") 'compile-plantuml-to-png)

(use-package yaml-mode
  :ensure t
  :mode "\\.yml\\'")

(use-package git-gutter-fringe :ensure t :defer 2)
  (add-hook 'prog-mode-hook 'git-gutter-mode)
  (add-hook 'org-mode-hook 'git-gutter-mode)
  (setq git-gutter-fr:side 'right-fringe)

(use-package define-word :ensure t :defer 2)

(defvar hidden-minor-modes
  ;; list of minor modes to hide form modeline
  '(abbrev-mode
    auto-revert-mode
    eldoc-mode
    undo-tree-mode
    evil-ledger-mode
    evil-smartparens-mode
    git-gutter-mode
    page-break-lines-mode
    yas-minor-mode
    ivy-mode
    company-mode
    which-key-mode
    flycheck-mode
    subword-mode
    ))

(defun purge-minor-modes ()
  ;; purges minor modes from modeline
  (interactive)
  (dolist (x hidden-minor-modes nil)
    (let ((trg (cdr (assoc x minor-mode-alist))))
      (when trg
	(setcar trg "")))))
(add-hook 'after-change-major-mode-hook 'purge-minor-modes)

(setq line-number-mode t)

(setcar mode-line-position "")

(defun byte-compile-init-dir ()
  "Byte-compile all your dotfiles."
  (interactive)
  (byte-recompile-directory user-emacs-directory 0))

(defun remove-elc-on-save ()
  "If you're saving an elisp file, likely the .elc is no longer valid."
  (add-hook 'after-save-hook
	    (lambda ()
	      (if (file-exists-p (concat buffer-file-name "c"))
		  (delete-file (concat buffer-file-name "c"))))
	    nil
	    t))

(add-hook 'emacs-lisp-mode-hook 'remove-elc-on-save)

(defun create-scratch-buffer nil
   "create a scratch buffer"
   (interactive)
   (switch-to-buffer (get-buffer-create "*scratch*"))
   (lisp-interaction-mode))

(defun create-org-buffer nil
   "create a org buffer"
   (interactive)
   (switch-to-buffer (get-buffer-create "*org*"))
   (org-mode)
   (olivetti-mode))

(defun create-restclient-buffer nil
   "create a restclient buffer"
   (interactive)
   (switch-to-buffer (get-buffer-create "*restclient*"))
   (restclient-mode))

(defun kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

(defun smcn/org-meta-return ()
  "This is a replacement for org mode's default M-RET functionality.
It will create a new text object underneath the current line, regardless of
how many characters are infront of the point."
  (interactive)
  (org-end-of-line)
  (org-meta-return))

(defun smcn/org-insert-todo-heading ()
  "This is a replacement for org mode's default S-M-RET functionality.
It will create a new TODO text object underneath the current line, regardless of
how many characters are infront of the point."
  (interactive)
  (org-end-of-line)
  (org-insert-todo-heading 0))

(defun magit-new-frame ()
  "Creates a magit-status buffer in a new frame"
  (interactive)
  (select-frame (new-frame))
  (call-interactively 'magit-status))

(global-set-key (kbd "C-x 5 g") 'magit-other-frame)

(defun kill-buffer-other-window ()
  "Kills the buffer in the other window.
  Will delete the window if the buffer underneath is the same as the current one."
  (interactive)
  (if (eq (buffer-name) (buffer-name (other-window -1)))
      (delete-window)
    (progn (kill-this-buffer)
	   (other-window -1)))
  (if (and (> (length (window-list)) 1)
	     (eq (buffer-name) (buffer-name (other-window -1))))
    (delete-window)
    (other-window -1)))

(global-set-key (kbd "C-x 4 k") 'kill-buffer-other-window)

(defun improved-kill-this-buffer ()
  "Kills the current buffer. 
  Deletes the window if the buffer undernearht is the same as the current one."
  (interactive)
  (kill-this-buffer)
  (if (eq (buffer-name) (buffer-name (other-window -1)))
    (delete-window)
    (other-window -1)))

(global-set-key (kbd "C-x k") 'improved-kill-this-buffer)

(defun eshell-other-window()
  "Opens/spawns an eshell buffer in the other window."
  (interactive)
  (split-window-sensibly)
  (other-window 1)
  (eshell))

(global-set-key (kbd "C-x 4 <f12>") 'eshell-other-window)

(defun time ()
  "Displays the current time in the minibuffer."
  (interactive)
  (message (format-time-string "%H:%M | %A the %d %B")))

(defun send-region-to-counsel-rg (region-beginning region-end)
  "Set the region as the initial value in a `counsel-rg` search."
  (interactive "r")
  (counsel-rg (buffer-substring region-beginning region-end)))

(defun send-region-to-counsel-grep-or-swiper (region-beginning region-end)
  "Set the region as the initial value in a `counsel-grep-or-swiper` search."
  (interactive "r")
  (counsel-grep-or-swiper (buffer-substring region-beginning region-end)))

(defun comments-only ()
  "Turn syntax-highlighting off, except for comments."
  (interactive)
  (let ((faces '(font-lock-builtin-face
		 font-lock-comment-delimiter-face
		 font-lock-constant-face
		 font-lock-doc-face
		 font-lock-function-name-face
		 font-lock-keyword-face
		 font-lock-negation-char-face
		 font-lock-preprocessor-face
		 font-lock-regexp-grouping-backslash
		 font-lock-regexp-grouping-construct
		 font-lock-string-face
		 font-lock-type-face
		 font-lock-variable-name-face
		 font-lock-warning-face)))
    (dolist (face faces)
      (face-remap-add-relative face '((:foreground "" :weight normal :slant normal))))
    (face-remap-add-relative 'font-lock-builtin-face '((:weight normal)))
    (face-remap-add-relative 'font-lock-constant-face '((:weight normal)))
    (face-remap-add-relative 'font-lock-function-name-face '((:slant normal)))
    (face-remap-add-relative 'font-lock-keyword-face '((:weight normal)))
    (face-remap-add-relative 'font-lock-preprocessor-face '((:weight normal)))
    (face-remap-add-relative 'font-lock-string-face '((:slant italic)))))

(defun current-project ()
  "Get the current project type."
  (interactive)
  (cond
   ((get-magento-root) "magento")
   ((get-laravel-root) "laravel")
   (t nil)))

(defun add-lisp-note ()
  "Open up a file within the note directory of a LISP project."
  (interactive)
  (let* ((root-dir (look-up-for '(".git" "src")))
	 (notes-dir (concat root-dir "/notes/")))
    (find-file-other-window (read-file-name "Choose note file: " notes-dir))
    (goto-char (point-max))))



(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/smcn.org"))

(defun config-reload ()
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/smcn.org")))

(defun push-password-store ()
  (interactive)
  (shell-command "cd ~/.password-store && git push"))

(defun edit-crontab ()
  "Edit the crontab. This works either locally or through TRAMP."
  (interactive)
  (with-editor-async-shell-command "crontab -e"))
