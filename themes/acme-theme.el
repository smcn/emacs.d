;;; acme-theme.el --- Port of "No Frils Acme" Vim theme.

;; Copyright (c) 2018 Stephen McNelly

;; This file is not part of GNU Emacs.

;;; Commentary:

;; Fork of Nofrils-Acme by Eric Sessoms.  Less syntax highlighting.
;; Only highlights comments and errors by default.  High-contrast
;; black-on-yellow and other colors inspired by Plan 9's Acme.

;; (require 'smcn-acme-theme)
;; (load-theme 'smcn-acme t)

;;; Credits:

;; This theme was ported from No Frils Acme by Robert Melton.
;; https://github.com/robertmeta/nofrils

;;; Code:

(deftheme acme
  "Port of No Frils Acme by Robert Melton.")

;; (let ((background "#FFFFD7")
(let ((background "#FFFFEA")
      (foreground "#000000")
      (comment "#000000")
      (error "#FF5555")
      (fringe "#EAFFFF")
      (search "#40883F")
      ;; (selection "#E7FF98")
      (selection "#eeee9e")
      ;; (status "#AEEEEE"))
      (status "#EAFFFF"))

  (custom-theme-set-faces
   'acme

   `(default ((t :background ,background :foreground ,foreground)))

   ;; Highlight only comments and errors.
   `(error ((t :background "white" :foreground ,error)))
   `(font-lock-builtin-face ((t nil)))
   `(font-lock-comment-face ((t :foreground ,comment :weight light :slant italic)))
   '(font-lock-comment-delimiter-face ((t :color ,comment :weight light :slant italic)))
   `(font-lock-constant-face ((t nil)))
   `(font-lock-function-name-face ((t nil)))
   `(font-lock-doc-face ((t :weight light :slant italic)))
   `(font-lock-keyword-face ((t nil)))
   `(font-lock-negation-char-face ((t nil)))
   `(font-lock-regexp-grouping-backslash ((t nil)))
   `(font-lock-regexp-grouping-construct ((t nil)))
   `(font-lock-string-face ((t nil)))
   `(font-lock-type-face ((t nil)))
   `(font-lock-variable-name-face ((t nil)))

   `(eldoc-highlight-function-argument ((t :weight normal)))

   ;; Show searches and selections.
   `(isearch ((t :background ,search :foreground "white")))
   `(lazy-highlight ((t :background ,error :foreground "white")))
   `(region ((t :background ,selection)))

   ;; Parenthesis matching is never wrong.
   ;; `(show-paren-match ((t :weight bold :background ,selection)))
   `(show-paren-match ((t :background ,foreground :foreground ,background)))
   `(show-paren-mismatch ((t :background ,error :weight bold)))
   ;; Decorate the frame to resemble Acme.
   `(fringe ((t :background ,fringe)))
   `(minibuffer-prompt ((t :foreground ,foreground)))
   `(mode-line ((t :background ,status)))
   `(mode-line-inactive ((t :background ,fringe)))

   ;; PHP specific faces
   `(php-php-tag ((t :slant normal)))
   `(php-function-name ((t :slant normal :weight normal)))
   `(php-function-call ((t :slant normal)))

   ;; Javascript faces
   `(js2-function-param ((t :foreground ,foreground)))
   `(js2-external-variable ((t :foreground ,foreground)))

   ;; Racket faces
   `(racket-selfeval-face ((t :forground ,foreground)))

   `(geiser-font-lock-repl-output ((t :foreground ,foreground)))
   
   ;; Org mode needs to chill.
   `(org-done ((t :weight bold)))
   `(org-todo ((t :weight bold)))))
   `(org-level-1 ((t :weight bold)))
   `(org-level-2 ((t :weight bold)))
   `(org-level-3 ((t :weight bold)))
   `(org-level-4 ((t :color "black" :weight bold)))
   `(org-level-5 ((t :weight bold)))
   `(org-level-6 ((t :weight bold)))

;;; Footer:

;;;###autoload
(when load-file-name
  (add-to-list
   'custom-theme-load-path
   (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'acme)

(provide 'acme-theme)

;;; acme-theme.el ends here
