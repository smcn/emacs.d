;;; naysayer-theme.el --- The naysayer color theme

;; Author: Stephen McNelly <stephenmcnelly@gmail.com>
;; Version: 0.2
;; Package-Version: 20190704.201
;; Filename: naysayer-theme.el
;; Package-Requires: ((emacs "24"))
;; License: GPL-3+

;;; Commentary:

;; Dark green blue color scheme with tan colors.  Inspired by Jonathan Blow's compiler livestreams.
;; Forked from https://github.com/nickav/naysayer-theme.el to remove syntax highlighting.

;;; Code:

(unless (>= emacs-major-version 24)
  (error "The naysayer theme requires Emacs 24 or later!"))

(deftheme naysayer "The naysayer color theme")

;; Monokai colors
(defcustom naysayer-theme-yellow "#E6DB74" "Primary colors - yellow" :type 'string :group 'monokai)
(defcustom naysayer-theme-orange "#FD971F" "Primary colors - orange" :type 'string :group 'monokai)
(defcustom naysayer-theme-red "#F92672" "Primary colors - red" :type 'string :group 'monokai)
(defcustom naysayer-theme-magenta "#FD5FF0" "Primary colors - magenta" :type 'string :group 'monokai)
(defcustom naysayer-theme-blue "#66D9EF" "Primary colors - blue" :type 'string :group 'monokai)
(defcustom naysayer-theme-green "#A6E22E" "Primary colors - green" :type 'string :group 'monokai)
(defcustom naysayer-theme-cyan "#A1EFE4" "Primary colors - cyan" :type 'string :group 'monokai)
(defcustom naysayer-theme-violet "#AE81FF" "Primary colors - violet" :type 'string :group 'monokai)

(let* ((background      "#0A1E24")
       (white           "#ffffff")
       (gutters         "#082628")
       (gutter-fg       'gutter-fg)
       (gutters-active  'gutter-fg)
       (builtin         'white)
       (selection       "#695285")
       (text            "#d2b58d")
       (comments        "#67cd5d")
       (punctuation     "#86E08F")
       (keywords        "#d2b58d")
       (variables       'white)
       (functions       'text)
       (methods         'text)
       (strings         "#2ec09c")
       (constants       "#8fe1c8")
       (macros          'punctuation)
       (error           "#ff0000")
       (warning         "#ffaa00")
       (highlight-line  "#0b3335")
       (line-fg         "#126367"))

  (custom-theme-set-faces
   'naysayer

   ;; default colors
   ;; *****************************************************************************

   `(default                                ((t (:foreground ,text :background ,background, :weight normal))))
   `(region                                 ((t (:foreground nil :background ,selection))))
   `(cursor                                 ((t (:foreground ,background :background ,white))))
   `(fringe                                 ((t (:background ,background :foreground ,white))))
   `(linum                                  ((t (:background ,background :foreground ,gutter-fg))))
   `(highlight                              ((t (:foreground nil :background ,selection))))
   `(error                                  ((t (:foreground ,text))))
   `(warning                                ((t (:foreground ,background :background ,warning))))

   ;; font lock faces
   ;; *****************************************************************************

   `(font-lock-keyword-face                 ((t (:foreground ,keywords, :weight normal))))
   `(font-lock-type-face                    ((t (:foreground ,text))))
   `(font-lock-constant-face                ((t (:foreground ,text))))
   `(font-lock-variable-name-face           ((t (:foreground ,text))))
   `(font-lock-builtin-face                 ((t (:foreground ,text))))
   `(font-lock-string-face                  ((t (:foreground ,strings))))
   `(font-lock-comment-face                 ((t (:foreground ,comments))))
   `(font-lock-comment-delimiter-face       ((t (:foreground ,comments))))
   `(font-lock-function-name-face           ((t (:foreground ,text))))
   `(font-lock-doc-string-face              ((t (:foreground ,text))))
   `(font-lock-preprocessor-face            ((t (:foreground ,text))))
   `(js2-external-variable                  ((t (:foreground ,text))))
   `(js2-function-param                     ((t (:foreground ,text))))
   `(org-table                              ((t (:foreground ,text))))
   `(org-block                              ((t (:foreground ,text))))
   `(show-paren-match                       ((t (:foreground ,background :background ,text))))

   ;; plugins
   ;; *****************************************************************************
   `(trailing-whitespace                    ((t (:foreground nil :background ,warning))))
   `(whitespace-trailing                    ((t (:background nil :foreground ,warning :inverse-video t))))

   `(web-mode-block-face                    ((t (:foreground nil :background nil))))

   `(linum                                  ((t (:foreground ,line-fg :background ,background))))
   `(linum-relative-current-face            ((t (:foreground ,white :background ,background))))
   `(line-number                            ((t (:foreground ,line-fg :background ,background))))
   `(line-number-current-line               ((t (:foreground ,white :background ,background))))

   ;; hl-line-mode
   ;; *****************************************************************************
   `(hl-line                                ((t (:background ,highlight-line))))
   `(hl-line-face                           ((t (:background ,highlight-line))))

   ;; rainbow-delimiters
   ;; *****************************************************************************
   `(rainbow-delimiters-depth-1-face        ((t (:foreground ,naysayer-theme-violet))))
   `(rainbow-delimiters-depth-2-face        ((t (:foreground ,naysayer-theme-blue))))
   `(rainbow-delimiters-depth-3-face        ((t (:foreground ,naysayer-theme-green))))
   `(rainbow-delimiters-depth-4-face        ((t (:foreground ,naysayer-theme-yellow))))
   `(rainbow-delimiters-depth-5-face        ((t (:foreground ,naysayer-theme-orange))))
   `(rainbow-delimiters-depth-6-face        ((t (:foreground ,naysayer-theme-red))))
   `(rainbow-delimiters-depth-7-face        ((t (:foreground ,naysayer-theme-violet))))
   `(rainbow-delimiters-depth-8-face        ((t (:foreground ,naysayer-theme-blue))))
   `(rainbow-delimiters-depth-9-face        ((t (:foreground ,naysayer-theme-green))))
   `(rainbow-delimiters-depth-10-face       ((t (:foreground ,naysayer-theme-yellow))))
   `(rainbow-delimiters-depth-11-face       ((t (:foreground ,naysayer-theme-orange))))
   `(rainbow-delimiters-depth-12-face       ((t (:foreground ,naysayer-theme-red))))

   ;; eshell
   ;; *****************************************************************************
   `(eshell-prompt                             ((t (:foreground ,comments :weight bold))))
   `(eshell-prompt-face                        ((t (:foreground ,comments :weight bold))))
   `(eshell-git-prompt-robyrussell-git-face    ((t (:foreground ,comments :weight bold))))
   `(eshell-git-prompt-robyrussell-git-face    ((t (:foreground ,comments :weight bold))))
   `(eshell-git-prompt-robyrussell-branch-face ((t (:foreground ,naysayer-theme-red :weight bold))))

   ;; gnus
   ;; *****************************************************************************
   `(gnus-summary-high-ancient-face         ((t (:foreground ,comments))))
   `(gnus-summary-high-unread-face          ((t (:foreground ,comments))))
   `(gnus-summary-high-read-face            ((t (:foreground ,comments))))
   `(gnus-summary-high-undownloaded-face    ((t (:foreground ,comments))))
   `(gnus-summary-high-ticked-face          ((t (:foreground ,comments))))

   `(gnus-summary-normal-ancient-face       ((t (:foreground ,text))))
   `(gnus-summary-normal-unread-face        ((t (:foreground ,text))))
   `(gnus-summary-normal-read-face          ((t (:foreground ,text))))
   `(gnus-summary-normal-undownloaded-face  ((t (:foreground ,text))))
   `(gnus-summary-normal-ticked-face        ((t (:foreground ,text))))

   `(gnus-summary-low-ancient-face          ((t (:foreground ,selection))))
   `(gnus-summary-low-unread-face           ((t (:foreground ,selection))))
   `(gnus-summary-low-read-face             ((t (:foreground ,selection))))
   `(gnus-summary-low-undownloaded-face     ((t (:foreground ,selection))))
   `(gnus-summary-low-ticked-face           ((t (:foreground ,selection))))

   ;; company
   ;; *****************************************************************************
   `(company-tooltip                        ((t (:background ,background :foreground ,text))))
   `(company-tooltip-selection              ((t (:foreground ,background :background ,text))))
   `(company-scrollbar-bg                   ((t (:background ,background))))
   `(company-scrollbar-fg                   ((t (:background ,text))))
   `(company-tooltip-common                 ((t (:background ,selection))))
   `(company-tooltip-common-selection       ((t (:background ,selection :foreground ,text))))

   ;; mode-line and powerline
   ;; *****************************************************************************
   `(sly-mode-line                          ((t (:foreground ,background :distant-foreground ,text :text))))
   `(mode-line-buffer-id                    ((t (:foreground ,background :distant-foreground ,text :text ,text :weight bold))))
   `(mode-line                              ((t (:inverse-video unspecified
							       :underline unspecified
							       :foreground ,background
							       :background ,text
							       :box nil))))
   `(powerline-active1                      ((t (:background ,text :foreground ,background))))
   `(powerline-active2                      ((t (:background ,text :foreground ,background))))

   `(mode-line-inactive                     ((t (:inverse-video unspecified
                                                               :underline unspecified
                                                               :foreground ,text
                                                               :background ,background
                                                               :box nil))))
   `(powerline-inactive1                    ((t (:background ,background :foreground ,text))))
   `(powerline-inactive2                    ((t (:background ,background :foreground ,text))))
  )

  (custom-theme-set-variables
    'naysayer
    '(linum-format " %5i ")
  )
)

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

;; *****************************************************************************

(provide-theme 'naysayer)

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'naysayer-theme)

;;; naysayer-theme.el ends here
